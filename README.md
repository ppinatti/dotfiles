# Dotfiles

Personal dotfiles (configuration files) repository with automatic installer
based on a yaml specification file.

Currently provides configuration for:
- bash
- screen
- tmux
- vim

# Quickstart

First install requirements:

```
$ pip3 install -r requirements.txt
```

Execute the installer:

```
$ ./install # you can provide a conf file as argument, see below
```

If no conf file is provided, the installer will use by default the file `install_conf.yaml`.

# Installer config file format

```yaml
global: # option section, defines default values
    # yes = overwrite existing files automatically
    # no = do not overwrite existing files and skip item
    # ask = ask for confirmation
    overwrite: [yes|no|ask]
    # link = create symlinks
    # copy = perform an actual copy
    method: [link|copy]

pre: # optional section, list of commands to execute before installing files
    - git submodule update --init # recommended due to vim plugins

items: # mandatory section, list of files/directories to install
    - src_path: vimrc # path to source file/directory
    - dst_path: ~/.vimrc # destination install path (absolute paths, tilde is supported)
```
