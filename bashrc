# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# User specific aliases and functions
set -o vi

# add new entries to the history instead of overwriting the whole file
shopt -s histappend

# show command line in tmux/screen's status line and title
if [ -t 1 ] && [ -n "$TMUX" -o ${TERM:0:6} == "screen" ]; then
    function update_name_title() {
        printf '\033k%s\033\\' "$*"
        # uncomment below to set the window's title too
        # printf '\033]0;%s\007' "$*"
    }

    # this trap enables the function to be executed before each command
    trap 'update_name_title "$BASH_COMMAND"' DEBUG

    # this is to return to the default when the command has completed
    default="$(ps -o comm= -p $$)"
    export PROMPT_COMMAND='update_name_title "$default"'
fi

# ensure every command is immediately saved to history file so that
# multiple instances can see it
export PROMPT_COMMAND="history -a; $PROMPT_COMMAND"

# my custom ssh function which does some nice tricks
function con() {
    # anything other than ssh host we pass to the original ssh
    if [ $# -ne 1 ]; then
        ssh $*
        return
    fi

    host=$1
    if [ `echo $host|awk -F'@' '{print NF-1}'` -eq 0 ]; then
        host="root@${host}"
    fi

    # display the hostname in tmux/screen's status line
    type -t update_name_title &>/dev/null
    if [ $? -eq 0 ]; then
        host_display=$(echo $host|awk -F'@' '{print $2}'|awk -F'.' '{print $1}')
        update_name_title "ssh:$host_display"
    fi

    sessions=$(ssh $host -q -t "screen -ls|awk '/(Detached)/ {print \$1}'")
    # apparently no screen available on host, just connect with default shell
    echo "$sessions"|grep -q "not found"
    if [ $? -eq 0 ]; then
        ssh $host
        return
    fi

    # no sessions detached: just connect with a new session
    if [ -z "$sessions" ]; then
        ssh $host -t "screen"
        return
    fi

    # not using tmux: only one session can be recovered
    if [ -z "$TMUX" ]; then
        ssh $host -t "screen -RR"
        return
    fi

    layout="-v"
    sessions=`echo $sessions|awk '{print NF-1}'`
    for i in `seq 1 $sessions`; do
        # -d is to keep focus on the current pane
        tmux split-window -d $layout "ssh $host -t 'screen -RR'"
        if [ $layout == "-v" ]; then
            layout="-h"
        else
            layout="-v"
        fi
        # this is needed to avoid two panes getting the same session
        sleep 0.2
    done
    ssh $host -t "screen -RR"
}

function gen_cscope() {
    local tmp_file=$(mktemp)
    [ -n "$tmp_file" ] || return 1
    local target_path="$HOME/.cscope/$(basename $PWD)"
    mkdir -p "$target_path" || return 1
    find -name '*.[chS]' \
        -o -name '*.x' \
    > "$tmp_file"
    local target_file="${target_path}/cscope.out"
    echo "generating cscope database in $target_file ..."
    cscope -b -R -q -i "$tmp_file" -f "$target_file"
    echo "done."
}
