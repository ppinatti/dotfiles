" load pathogen (not needed anymore :))
" runtime bundle/vim-pathogen/autoload/pathogen.vim
" execute pathogen#infect()

" combinations that I usually mistake
cab W  w
cab w  w!
cab Q  q
cab X  x
cab x  x!

" shortcut to split commands
cab s split
cab v vsplit

" when editing long lines with wrapping enabled don't skip over the current line
nnoremap j gj
nnoremap k gk

" shortcuts to move between buffers
nnoremap <silent> L :bnext<CR>
nnoremap <silent> H :bprevious<CR>
nnoremap <silent> <C-c> :bdelete<CR>

" shortcut to clear search history
nmap <silent> ,/ :nohlsearch<CR>

" This mapping uses <cword> to get the word under the cursor, and searches for
" it in the current directory and all subdirectories, opening the quickfix
" window when done (see https://vim.fandom.com/wiki/Find_in_files_within_Vim)
nnoremap <C-g> :execute "noautocmd vimgrep /" . expand("<cword>") . "/j **" <Bar> cw<CR>

" nice shortcut to format text paragraphs (put lines together)
nnoremap = gqap
vmap = gq

" accelerator to toggle paste mode
set pastetoggle=<leader>p

" colors for vim
syn on
set bg=dark
" enable true color under tmux/screen
if &term =~# '^screen'
  let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
  let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
endif

" open other files without having to save/undo changes to current one
set hidden

" highlight tabs, last column and first column on nowrap, trailing spaces, non printable space
set list lcs=tab:>-,extends:>,precedes:<,trail:-,nbsp:-

" open on right side on vertical split
set splitright

" command-line completion (first tab completes, second list possible matches)
set wildmode=longest,list

" smart search options
set ignorecase " ignore case when searching
set smartcase  " ignore case if search is all lowercase, case-sensitive otherwise
set hlsearch   " highlight search terms
set incsearch  " show search matches as you type

" indenting configuration {

" allow vim to load indent files based on file type (also allows filetype
" detection and plugin loading)
filetype plugin indent on

" autoindent - follow indentation from existing line when creating a new one
set ai
" copyindent - use the exact same characters when autoindenting
set ci
" smartindent - adds one level of indentation on specific cases
" currently not setting to not interfere with auto indent by file type
"set si

" powerful backspacing (join lines, also affects Del key)
set bs=indent,eol,start

" switch tab for space
set expandtab

" tab character width in spaces on screen (8 = default value)
set tabstop=4

" how many spaces for indent level (i.e. when indenting with >> and <<)
set shiftwidth=4

" how many spaces to use when tab or backspace is pressed
set softtabstop=4

" do not wrap long lines
set nowrap

" disable those vi: strings containing option settings
set nomodeline

" c language {

" (0 = when in unclosed parens, indent 0 chars from the line with the unclosed parens
" :0 = place case labels 0 chars from the indent of the switch()
" l1 = aligh with a case label
" t0 = indent function return type declaration 0 chars from the margin
" L3 = indent jump label minus 3 chars
autocmd filetype c setlocal cinoptions=(0,:0,l1,t0,L3

" c language }

" make/automake {

autocmd FileType make setlocal noexpandtab
autocmd BufRead,BufNewFile *.am setlocal noexpandtab

" make/automake }

" python {

" text width with 80 columns
autocmd filetype python setlocal tw=79

" python }

" html,xml {

" how many spaces for indent level (i.e. when indenting with >> and <<)
autocmd filetype html setlocal shiftwidth=2
autocmd filetype xml setlocal shiftwidth=2

" how many spaces to use when tab or backspace is pressed
autocmd filetype html setlocal softtabstop=2
autocmd filetype xml setlocal softtabstop=2

" html,xml }

" yaml,yml {

" how many spaces for indent level (i.e. when indenting with >> and <<)
autocmd filetype yaml setlocal shiftwidth=2
autocmd filetype yml setlocal shiftwidth=2

" how many spaces to use when tab or backspace is pressed
autocmd filetype yaml setlocal softtabstop=2
autocmd filetype yml setlocal softtabstop=2

" yaml,yml }

" indenting configuration }

" plugin tagbar {

nnoremap <silent> <leader>t :TagbarToggle<CR>

" automatically open tagbar, if it supports buffer
" autocmd BufEnter * nested :TagbarOpen
" autocmd BufLeave * nested :TagbarClose
"autocmd BufLeave * nested :call tagbar#autoopen(0)

"
" autocmd BufLeave * call s:TagbarMarkBuffer()
" autocmd BufEnter * call s:TagbarAutoOpen()

" window's width
let g:tagbar_width = 25

" how many spaces per indentation level
let g:tagbar_indent = 1

" compact mode, saves screen space
let g:tagbar_compact = 1

" sort tags by order in file instead of name
let g:tagbar_sort = 0

" plugin tagbar }

" vimwiki plugin {

" set different colors for headers for better reading
highlight VimwikiHeader1 ctermfg=darkmagenta
highlight VimwikiHeader2 ctermfg=darkyellow
highlight VimwikiHeader3 ctermfg=darkgreen
highlight VimwikiHeader4 ctermfg=lightmagenta
highlight VimwikiHeader5 ctermfg=yellow
highlight VimwikiHeader6 ctermfg=lightgreen

" work wiki {
let wiki_1 = {}
let wiki_1.ext = '.md'
let wiki_1.path = '~/work/vimwiki'
let wiki_1.path_html = '~/work/vimwiki/html'
let wiki_1.syntax = 'markdown'
let wiki_1.template_path = '~/work/vimwiki/html/templates'
let wiki_1.template_default = 'default'
let wiki_1.template_ext = '.html'
let wiki_1.custom_wiki2html = '~/pessoal/bin/marked_md2html.py'

" auto generate html when wiki is saved
let wiki_1.auto_export = 1

" syntax highlight when using {{{language
let wiki_1.nested_syntaxes = {'python': 'python', 'c++': 'cpp', 'c': 'c'}

" work wiki }

" personal wiki {
let wiki_2 = {}
let wiki_2.ext = '.md'
let wiki_2.path = '~/pessoal/vimwiki'
let wiki_2.path_html = '~/pessoal/vimwiki/html'
let wiki_2.syntax = 'markdown'
let wiki_2.template_path = '~/pessoal/vimwiki/html/templates'
let wiki_2.template_default = 'default'
let wiki_2.template_ext = '.html'
let wiki_2.custom_wiki2html = '~/pessoal/bin/marked_md2html.py'

" auto generate html when wiki is saved
let wiki_2.auto_export = 1

" syntax highlight when using {{{language
let wiki_2.nested_syntaxes = {'python': 'python', 'c++': 'cpp', 'c': 'c'}

" personal wiki }

" add wikis to main configuration
let g:vimwiki_list = [wiki_1, wiki_2]
" make sure templates are not deleted
let g:vimwiki_user_htmls = 'templates/default.html'

" vimwiki plugin }

" sparkup (create html using css) plugin {
"let g:sparkupExecuteMapping = '<c-e>'
" sparkup plugin }

" unite plugin {

" uniteplugin }

" netrw {

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 4
let g:netrw_winsize = 25
nnoremap <silent> <leader>e :Lexplore!<CR>

" netrw }

" airline plugin {

" use powerline symbols
let g:airline_powerline_fonts = 1

" uncomment this to show buffer list at the top
let g:airline#extensions#tabline#enabled = 1

" disable integration with gitgutter
"let g:airline#extensions#hunks#enabled = 0

" make the sign column integrate better with airline colors
highlight clear SignColumn
highlight link SignColumn airline_x

" always show statusline
set laststatus=2

" don't report tagbar and filetype
"let g:airline_section_x = ''

" don't report line percent value
function! AirlineInit()
    let g:airline_section_z = airline#section#create(['linenr', ':%3c '])
endfunction
autocmd VimEnter * call AirlineInit()

" save tmux colors to this file whenever airline theme is changed and
" on vim startup
"let airline#extensions#tmuxline#snapshot_file = "~/.tmux-statusline-colors.conf"

" don't change tmux colors
let g:airline#extensions#tmuxline#enabled = 0

" airline plugin }

" ale plugin {

let g:ale_linters = {'python': ['pylint']}

let g:ale_echo_msg_format = '[%linter%] %code%: %s'

let g:ale_completion_enabled = 1
" When working with TypeScript files, ALE supports automatic imports from
" external modules. This behavior is disabled by default and can be enabled by
" the setting below.
let g:ale_completion_tsserver_autoimport = 0

" ale plugin }

" gitgutter plugin {

" mapping to turn plugin on/off
nnoremap <silent> <leader>g :GitGutterToggle<CR>

" turn off by default
let g:gitgutter_enabled = 0

" turn on highlighted lines
let g:gitgutter_highlight_lines = 1

" colors for highlighted lines
highlight GitGutterAddLine term=bold ctermbg=10 guibg=LightGreen
highlight link GitGutterChangeDeleteLine GitGutterChangeLine
highlight GitGutterDeleteLine term=bold ctermbg=9 guibg=LightRed

" gitgutter plugin }

" markdown built-in plugin {

let g:markdown_fenced_languages = ['html', 'python', 'bash=sh']
let g:markdown_syntax_conceal = 0
let g:markdown_minlines = 100

" markdown built-in plugin }

" jinja syntax highlighting
autocmd BufRead,BufNewFile *.jinja setlocal filetype=jinja
autocmd filetype jinja setlocal syntax=jinja
